﻿using System;
using System.IO;
using System.Text;

namespace MSELF_Tool
{
    class MSELF
    {
        struct mself_header
        {
            internal uint m_magic; // m_magic is used to identify the file as an MSELF. The magic is 0x4D534600.
            internal uint m_format_version; // m_format_version is the version of the MSELF format. Currently the valid version is 1.
            internal ulong m_file_size; // m_file_size stores the file size of MSELF. The file size includes the mself_header and mself_entry block, all the padding, and all the internal files.
            internal uint m_entry_num; // m_entry_num stores the number of internal files in the MSELF. This is the same as the number of mself_entry entries immediately after mself_header. Padding is not included.
            internal uint m_entry_size; // m_entry_size stores the size of one mself_entry in bytes. Immediately after mself_header, there will be m_entry_size * m_entry_num bytes of mself_entry.
            internal byte[] m_reserve; // m_reserve is an area reserved for future extension. In MSELF Version 1, the size of this area (MSELF_HEADER_RESERVE_SIZE) is 40. This area is not used in Version 1, but must be filled with zeroes.
        };

        struct mself_entry
        {
            internal string m_name; // m_name stores the internal filename (terminated with 0x00) to identify an internal file. If the number of characters is less than 31, the remaining space within m_name must each be 0x00.
            internal ulong m_offset; // m_offset stores the offset from the start of MSELF to the start of the internal file. This offset includes mself_header and mself_entry.
            internal ulong m_size; // m_size stores the size of an internal file in bytes. If the sum of m_offset and m_size is less than m_offset of the next mself_entry minus 1, the space will be filled with padding.
            internal byte[] m_reserve; // m_reserve is an area reserved for future extension. In MSELF Version 1, the size of this area (MSELF_ENTRY_RESERVE_SIZE) is 16. This area is not used in Version 1, but must be filled with zeroes.
        };

        internal string[] ListEntries(string mselfFile)
        {
            string[] results;

            using (FileStream fs = new FileStream(mselfFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
            {
                var header = ReadHeader(fs);
                if (header.m_reserve == null)
                    return new string[] { "Error reading the MSELF file. Invalid magic!" };

                mself_entry[] entries = new mself_entry[header.m_entry_num];

                for (int i = 0; i < header.m_entry_num; i++)
                    entries[i] = ReadEntries(fs);

                results = new string[entries.Length];

                for (int i = 0; i < entries.Length; i++)
                    results[i] = entries[i].m_name + ";" + entries[i].m_offset + ";" + entries[i].m_size;
            }

            return results;
        }

        internal void ExtractEntries(string mselfFile, string targetDirectory, bool partial = false)
        {
            using (FileStream fs = new FileStream(mselfFile, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
            {
                Console.WriteLine("Reading TOC");
                var header = ReadHeader(fs);
                Console.WriteLine("Found " + header.m_entry_num + " entries" + Environment.NewLine);
                mself_entry[] entries = new mself_entry[header.m_entry_num];

                for (int i = 0; i < header.m_entry_num; i++)
                    entries[i] = ReadEntries(fs);

                string[] contents = null;
                if (partial)
                {
                    Console.WriteLine("Found " + Path.GetFileName(mselfFile) + "_jobs.txt");
                    contents = File.ReadAllLines(mselfFile + "_jobs.txt");
                    Console.WriteLine("Found " + contents.Length + " jobs" + Environment.NewLine);
                }

                ulong count = 0;
                int hasExtractedCount = 1;

                foreach (mself_entry entry in entries)
                {
                    // Some begin with a slash, while some others don't. This can be troublesome, so remove it if it's present
                    string entryFilename = entry.m_name;
                    if (entryFilename[0] == '/')
                        entryFilename = entryFilename.Substring(1);

                    count++;

                    Console.WriteLine("Processing entry " + count + " out of " + entries.Length + " entries");

                    if (partial)
                    {
                        bool skip = true;

                        foreach (var item in contents)
                        {
                            if (item == entry.m_name + ';' + entry.m_offset + ';' + entry.m_size)
                            {
                                skip = false;
                                break;
                            }
                            else if (!item.Contains(";") && item.Contains(entryFilename))
                            {
                                skip = false;
                                break;
                            }
                            else if (item.Contains("*"))
                            {
                                string[] ext = item.Split('.');
                                if (entryFilename.Contains(ext[ext.Length - 1]))
                                {
                                    skip = false;
                                    break;
                                }
                            }
                        }

                        if (skip)
                            continue;
                    }

                    if (entryFilename.Contains("/"))
                    {
                        string[] parts = entryFilename.Split(new char[] { '/' });

                        for (int i = 0; i < parts.Length - 1; i++)
                            if (!Directory.Exists(targetDirectory + parts[i]))
                                Directory.CreateDirectory(targetDirectory + parts[i]);

                        parts[parts.Length - 1] = string.Format("{0:000000}_{1}", count, parts[parts.Length - 1]);

                        entryFilename = string.Join("/", parts);
                    }
                    else
                        entryFilename = string.Format("{0:000000}_{1}", count, entryFilename);

                    Console.WriteLine("Extracting " + entryFilename + Environment.NewLine);

                    using (FileStream entryFS = new FileStream(targetDirectory + entryFilename, FileMode.Create, FileAccess.ReadWrite, FileShare.None, 4096, FileOptions.SequentialScan))
                    {
                        fs.Position = (long)entry.m_offset;
                        
                        byte[] buffer = new byte[4096];
                        int bytesRead = 0;
                        long totalRead = 0;
                        while ((bytesRead = fs.Read(buffer, 0, buffer.Length)) > 0)
                        {
                            totalRead += bytesRead;

                            if (totalRead < (long)entry.m_size)
                                entryFS.Write(buffer, 0, bytesRead);
                            else if ((long)entry.m_size > buffer.Length)
                            {
                                entryFS.Write(buffer, 0, bytesRead - (int)(totalRead - (long)entry.m_size));
                                break;
                            }
                            else
                            {
                                entryFS.Write(buffer, 0, (int)entry.m_size);
                                break;
                            }
                        }

                        hasExtractedCount++;
                    }
                }

                hasExtractedCount--; // Undo the final addition

                if (hasExtractedCount == entries.Length)
                    Console.WriteLine("All files extracted ({0} out of {1})", hasExtractedCount, entries.Length);
                else
                    Console.WriteLine("Extracted {0} out of {1} files", hasExtractedCount, entries.Length);
            }
        }

        internal void UpdateEntries(string mselfFile, string targetDirectory)
        {
            int baseFilename = targetDirectory.Length;

            using (FileStream fs = new FileStream(mselfFile, FileMode.Open, FileAccess.ReadWrite, FileShare.Read, 4096, FileOptions.RandomAccess))
            using (BinaryWriter writer = new BinaryWriter(fs))
            {
                var header = ReadHeader(fs);

                mself_entry[] entries = new mself_entry[header.m_entry_num];

                for (int i = 0; i < header.m_entry_num; i++)
                    entries[i] = ReadEntries(fs);

                string[] files = Directory.GetFiles(targetDirectory, "*", SearchOption.AllDirectories);
                
                Console.WriteLine("Found " + files.Length + " files" + Environment.NewLine);
                
                int count = 0;
                int hasUpdatedCount = 1;

                foreach (string file in files)
                {
                    count++;

                    Console.WriteLine("Processing file " + count + " out of " + files.Length + " files");

                    FileInfo fi = new FileInfo(file);

                    string filename = file;

                    if (filename.Contains("\\"))
                    {
                        filename = file.Substring(baseFilename);
                        string[] filenameParts = filename.Split(new string[] { "\\" }, StringSplitOptions.None);
                        filename = filenameParts[filenameParts.Length - 1];
                    }
                    else
                        filename = file.Substring(baseFilename);

                    long fileIndex;
                    if (!long.TryParse(filename.Substring(0, 6), out fileIndex) || !entries[fileIndex - 1].m_name.Contains(filename.Substring(7)))
                    {
                        Console.WriteLine(filename + " file entry not found in the TOC - updating of " + Path.GetFileName(mselfFile) + " is skipped" + Environment.NewLine);
                        continue;
                    }

                    Console.WriteLine("Found " + filename + " file entry in the TOC");
                    
                    var entry = entries[fileIndex - 1];

                    if ((ulong)fi.Length > entry.m_size)
                    {
                        Console.WriteLine(filename + " file size is bigger than the TOC entry - updating of " + Path.GetFileName(mselfFile) + " is skipped" + Environment.NewLine);
                        continue;
                    }

                    Console.WriteLine(filename + " file size is smaller than or equal to the TOC entry");

                    entries[fileIndex - 1].m_size = (ulong)fi.Length;

                    fs.Position = (long)entry.m_offset;

                    Console.WriteLine("Updating " + filename + " entry" + Environment.NewLine);

                    using (FileStream fileFS = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read, 4096, FileOptions.SequentialScan))
                    {
                        byte[] buffer = new byte[4096];

                        int readBytes = 0;
                        
                        while ((readBytes = fileFS.Read(buffer, 0, buffer.Length)) > 0)
                            writer.Write(buffer, 0, readBytes);

                        // Add padding after the file's contents
                        if (fileIndex != entries.Length)
                            buffer = new byte[entries[fileIndex].m_offset - (ulong)fs.Position];
                        else
                            buffer = new byte[fs.Length - fs.Position];

                        writer.Write(buffer);
                    }

                    hasUpdatedCount++;
                }

                fs.Position = 0;

                byte[] tmp;

                tmp = BitConverter.GetBytes(header.m_magic);
                Array.Reverse(tmp);
                writer.Write(tmp);

                tmp = BitConverter.GetBytes(header.m_format_version);
                Array.Reverse(tmp);
                writer.Write(tmp);

                tmp = BitConverter.GetBytes(header.m_file_size);
                Array.Reverse(tmp);
                writer.Write(tmp);

                tmp = BitConverter.GetBytes(header.m_entry_num);
                Array.Reverse(tmp);
                writer.Write(tmp);

                tmp = BitConverter.GetBytes(header.m_entry_size);
                Array.Reverse(tmp);
                writer.Write(tmp);

                writer.Write(header.m_reserve); // It's null, so there's no need to reverse this one

                foreach (var entry in entries)
                {
                    byte[] entryName = Encoding.ASCII.GetBytes(entry.m_name);

                    writer.Write(entryName);
                    writer.Write(new byte[32 - entryName.Length]); // Filename padding

                    tmp = BitConverter.GetBytes(entry.m_offset);
                    Array.Reverse(tmp);
                    writer.Write(tmp);    

                    tmp = BitConverter.GetBytes(entry.m_size);
                    Array.Reverse(tmp);
                    writer.Write(tmp);

                    writer.Write(entry.m_reserve); // It's null, so there's no need to reverse this one
                }

                hasUpdatedCount--; // Undo the last addition

                if (hasUpdatedCount == entries.Length)
                    Console.WriteLine("All files in the MSELF updated ({0} out of {1})", hasUpdatedCount, files.Length);
                else
                    Console.WriteLine("Updated {0} out of {1} files in the MSELF", hasUpdatedCount, files.Length);
            }
        }

        private mself_header ReadHeader(FileStream fs)
        {
            mself_header mselfHeader = new mself_header();

            byte[] buffer = new byte[sizeof(ulong)];

            fs.Read(buffer, 0, sizeof(uint));
            Array.Reverse(buffer, 0, sizeof(uint));
            mselfHeader.m_magic = BitConverter.ToUInt32(buffer, 0);

            if (mselfHeader.m_magic.ToString("X") != "4D534600")
                return new mself_header();

            fs.Read(buffer, 0, sizeof(uint));
            Array.Reverse(buffer, 0, sizeof(uint));
            mselfHeader.m_format_version = BitConverter.ToUInt32(buffer, 0);

            fs.Read(buffer, 0, sizeof(ulong));
            Array.Reverse(buffer, 0, sizeof(ulong));
            mselfHeader.m_file_size = BitConverter.ToUInt64(buffer, 0);

            fs.Read(buffer, 0, sizeof(uint));
            Array.Reverse(buffer, 0, sizeof(uint));
            mselfHeader.m_entry_num = BitConverter.ToUInt32(buffer, 0);

            fs.Read(buffer, 0, sizeof(uint));
            Array.Reverse(buffer, 0, sizeof(uint));
            mselfHeader.m_entry_size = BitConverter.ToUInt32(buffer, 0);

            mselfHeader.m_reserve = new byte[0x28]; // m_reserve should be 0x28 bytes
            fs.Seek(0x28, SeekOrigin.Current); // so we skip them

            return mselfHeader;
        }

        private mself_entry ReadEntries(FileStream fs)
        {
            mself_entry mselfEntry = new mself_entry();

            byte[] buffer = new byte[sizeof(ulong) * 4];

            fs.Read(buffer, 0, sizeof(ulong) * 4);
            mselfEntry.m_name = Encoding.ASCII.GetString(buffer).Replace("\0", "");

            fs.Read(buffer, 0, sizeof(ulong));
            Array.Reverse(buffer, 0, sizeof(ulong));
            mselfEntry.m_offset = BitConverter.ToUInt64(buffer, 0);

            fs.Read(buffer, 0, sizeof(ulong));
            Array.Reverse(buffer, 0, sizeof(ulong));
            mselfEntry.m_size = BitConverter.ToUInt64(buffer, 0);

            mselfEntry.m_reserve = new byte[0x10]; // m_reserve should be 0x10 bytes
            fs.Seek(0x10, SeekOrigin.Current); // so we skip them

            return mselfEntry;
        }
    }
}
