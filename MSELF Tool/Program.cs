﻿using MSELF_Tool.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

namespace MSELF_Tool
{
    class Program
    {
        static MSELF mself = new MSELF();

        static void Main(string[] args)
        {
            //int padding32BAlignment = (test.Length + 31) & ~31;

            if (!Directory.Exists(Resources.TargetDir))
                Directory.CreateDirectory(Resources.TargetDir);

            foreach (string arg in args)
            {
                if (arg == "-l" || arg == "--list")
                {
                    ListEntries();
                    Console.WriteLine(Resources.FinishListEntries);
                    return; // We won't be using multiple parameters in this application, so stop scanning for more when one is found
                }
                else if (arg == "-e" || arg == "--extract")
                {
                    ExtractEntries();
                    return;
                }
                else if (arg == "-u" || arg == "--update")
                {
                    UpdateEntries();
                    return;
                }
                else
                {
                    FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                    Console.WriteLine(string.Format("No valid parameters.{0}\"{1}\" -h or \"{1}\" --help for usage instructions.", Environment.NewLine, Path.GetFileNameWithoutExtension(fvi.FileName)));
                    return;
                }
            }

            if (args.Length == 0)
            {
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location);

                Console.WriteLine("{0} {1} by {2}, written for catalinnc." + Environment.NewLine, fvi.ProductName, fvi.ProductVersion, fvi.CompanyName);

                Console.WriteLine("  -e  --extract{0,63}", "Extracts the requested entries if mself_jobs.txt is found,");
                Console.WriteLine("{0,70}", "otherwise, all the entries in mself are extracted.");
                Console.WriteLine("  -l  --list{0,66}", "Writes entry information for all mself files in the target");
                Console.WriteLine("{0,74}", "directory into mself_toc.txt and mself_toc_summary.txt");
                Console.WriteLine("  -u  --update{0,65}", "Writes all valid TOC entry files found in the target folder");
                Console.WriteLine("{0,54}", "back into its matching mself file.");
            }
        }

        static void ListEntries()
        {
            foreach (string mselfFile in Directory.GetFiles(Resources.TargetDir, "*.mself", SearchOption.AllDirectories))
            {
                Console.WriteLine("Found " + Path.GetFileName(mselfFile) + Environment.NewLine);

                Console.WriteLine("Reading TOC");
                string[] entries = mself.ListEntries(mselfFile);

                Console.WriteLine("Saving " + Path.GetFileName(mselfFile) + "_toc.txt");
                File.WriteAllLines(mselfFile + "_toc.txt", entries);

                Console.WriteLine("Analyzing TOC");
                string tocStats = GenerateTOCStatistics(entries);

                Console.WriteLine("Saving " + Path.GetFileName(mselfFile) + "_toc_summary.txt" + Environment.NewLine + Environment.NewLine);
                File.WriteAllText(mselfFile + "_toc_summary.txt", tocStats);
            }
        }

        static void ExtractEntries()
        {
            foreach (string mselfFile in Directory.GetFiles(Resources.TargetDir, "*.mself", SearchOption.AllDirectories))
            {
                Console.WriteLine("Found " + Path.GetFileName(mselfFile) + Environment.NewLine);

                if (!Directory.Exists(Path.GetDirectoryName(mselfFile) + "/" + Path.GetFileNameWithoutExtension(mselfFile)))
                    Directory.CreateDirectory(Path.GetDirectoryName(mselfFile) + "/" + Path.GetFileNameWithoutExtension(mselfFile));

                bool partial = false;
                if (File.Exists(mselfFile + "_jobs.txt"))
                    partial = true;
                mself.ExtractEntries(mselfFile, Path.GetDirectoryName(mselfFile) + "/" + Path.GetFileNameWithoutExtension(mselfFile) + "/", partial);
            }
        }

        static void UpdateEntries()
        {
            foreach (string mselfFile in Directory.GetFiles(Resources.TargetDir, "*.mself", SearchOption.AllDirectories))
            {
                Console.WriteLine("Found " + Path.GetFileName(mselfFile) + Environment.NewLine);

                if (!Directory.Exists(Resources.TargetDir + "/" + Path.GetFileNameWithoutExtension(mselfFile)))
                {
                    Console.WriteLine(Path.GetFileNameWithoutExtension(mselfFile) + " not found - updating of " + Path.GetFileName(mselfFile + " is skipped") + Environment.NewLine + Environment.NewLine);
                    continue;
                }

                Console.WriteLine("Found " + Path.GetFileNameWithoutExtension(mselfFile));
                mself.UpdateEntries(mselfFile, Resources.TargetDir + "/" + Path.GetFileNameWithoutExtension(mselfFile) + "/");
            }
        }

        private static string GenerateTOCStatistics(string[] entries)
        {
            HashSet<string> seen = new HashSet<string>();
            List<string> repeat = new List<string>();

            uint selfCount = 0;
            uint sprxCount = 0;
            uint sdatCount = 0;

            foreach (string entry in entries)
            {
                int nameEnd = entry.IndexOf(';');
                string name = entry.Remove(nameEnd);

                if (!seen.Contains(name))
                    seen.Add(name);
                else
                {
                    repeat.Add(name);
                    continue;
                }

                if (name.ToLower().EndsWith(".self"))
                    selfCount++;
                else if (name.ToLower().EndsWith(".sprx"))
                    sprxCount++;
                else if (name.ToLower().EndsWith(".sdat"))
                    sdatCount++;
            }

            List<string> final = new List<string>();

            foreach (string entry in entries)
            {
                int nameEnd = entry.IndexOf(';');
                string name = entry.Remove(nameEnd);

                if (repeat.Contains(name))
                    final.Add(entry);
            }

            final.Sort();

            StringBuilder sb = new StringBuilder();
            char s = ' '; // space
            sb.AppendLine(Resources.TotalEntries + s + entries.Length);
            sb.AppendLine(Resources.UniqueEntries + s + seen.Count);
            sb.AppendLine();
            sb.AppendLine(Resources.TotalSELFs + s + selfCount);
            sb.AppendLine(Resources.TotalSPRXs + s + sprxCount);
            sb.AppendLine(Resources.TotalSDATs + s + sdatCount);
            sb.AppendLine();
            sb.AppendLine();
            sb.AppendLine(Resources.NonUniqueList);
            sb.AppendLine();
            string previous = null;
            foreach (string entry in final)
            {
                int nameEnd = entry.IndexOf(';');
                string name = entry.Remove(nameEnd);

                if (previous != name)
                {
                    sb.AppendLine(entry);
                    previous = name;
                }
                else
                    sb.AppendLine(entry + Environment.NewLine);
            }

            return sb.ToString();
        }
    }
}
